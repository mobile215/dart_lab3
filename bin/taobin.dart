import 'dart:io';

void main() {
  print("touch screen\n");
  int sum_cart = 0;
  int? menu2, level2;
  String? add;
  int? money1 = 0;

  List<Map<String, int>> menu = [
    {"Hot Coffee": 35},
    {"Iced Coffee": 40},
    {"Hot Tea": 35},
    {"Iced Tea": 40},
    {"Hot Milk": 40},
    {"Ice Milk": 45}
  ];

  List<String> sweet_level = [
    "No Sugar 0%",
    "Less Sweet 25%",
    "Just right 50%",
    "Sweet 75%",
    "Very Sweet 100%",
  ];

  List<String> add_cart = [];

  List<String> payment = [
    "เงินสด",
    "สแกน QR",
    "ใช้เต่าบินเครดิต",
    "ใช้คูปอง/ดูคูปองสะสม"
  ];

  while (true) {
    add = selectPD(menu, menu2, sweet_level, level2, add, add_cart);
    if (add == "no") {
      break;
    }
    sum_cart = list(add_cart, sum_cart, menu, sweet_level);
    checkpayment(payment, money1, sum_cart);
    print("\nกรุณารอสักครู่ ระบบกำลังเตรียมสินค้าของท่าน");
    for (int i = (6 * add_cart.length); i >= 0; i--) {
      sleep(Duration(seconds: 1));
      print("เหลือเวลา $i วินาที");
    }
    print("ระบบดำเนินการสำเร็จ ขอบคุณที่ใช้บริการ Taobin");
  }
}

String? selectPD(List<Map<String, int>> menu, int? menu2,
    List<String> sweet_level, int? level2, String? add, List<String> add_cart) {
  for (int i = 0; i < menu.length; i++) {
    print(
        "${i + 1} = ${menu[i].keys.first} : ราคา ${menu[i].values.first} บาท");
  }
  print("\nกรุณาเลือกเมนู");
  while (true) {
    menu2 = int.parse(stdin.readLineSync()!);

    if (menu2 < 1 || menu2 > menu.length) {
      print("ไม่มีเมนูในระบบ กรุณาใส่หมายเลขใหม่");
    } else {
      break;
    }
  }
  print("\n");

  for (int i = 0; i < sweet_level.length; i++) {
    print("${i + 1} = ${sweet_level[i]}");
  }
  print("\nกรุณาเลือกระดับความหวาน");
  while (true) {
    level2 = int.parse(stdin.readLineSync()!);
    if (level2 < 1 || level2 > menu.length) {
      print("ไม่มีระดับความหวานในระบบ กรุณาใส่หมายเลขใหม่");
    } else {
      break;
    }
  }
  add_cart.add("${menu2 - 1},${level2 - 1}");
}

int list(List<String> add_cart, int sum_cart, List<Map<String, int>> menu,
    List<String> sweet_level) {
  print("\nรายการทั้งหมดของท่าน");
  for (int i = 0; i < add_cart.length; i++) {
    List<String> add1 = add_cart[i].split(",");
    sum_cart += menu[int.parse(add1[0])].values.first;
    print(
        "${i + 1} = ${menu[int.parse(add1[0])].keys.first} ระดับความหวานที่ท่านเลือก ${sweet_level[int.parse(add1[1])]} ราคา ${menu[int.parse(add1[0])].values.first} บาท ");
  }

  print("รวมทั้งหมด ${sum_cart} บาท\n");
  return sum_cart;
}

void checkpayment(List<String> payment, int? money1, int sum_cart) {
  while (true) {
    for (int i = 0; i < payment.length; i++) {
      print("${i + 1} = ${payment[i]}");
    }
    print("\nกรุณาเลือกวิธีชำระเงิน");

    var payment1 = stdin.readLineSync()!;
    while (money1! > -1) {
      if (payment1 == "1") {
        print("ชำระโดยเงินสด");
        money1 = int.parse(stdin.readLineSync()!);
        if (money1 >= sum_cart) {
          print("ชำระเงินสำเร็จ");
          if (money1 > sum_cart) {
            print("เงินทอน ${money1 - sum_cart} บาท");
          }
          break;
        } else {
          print(
              "จำนวนเงินของท่านไม่เพียงพอ กรุณาชำระเงินใหม่ (โดยเปลี่ยนการชำระ กดหมายเลย '-1");
        }
      } else if (payment1 == '2') {
        print("สแกน QR");
        money1 = int.parse(stdin.readLineSync()!);

        if (money1 >= sum_cart) {
          print("ชำระเงินสำเร็จ");
          break;
        } else {
          print(
              "จำนวนเงินของท่านไม่เพียงพอ กรุณาชำระเงินใหม่ (โดยเปลี่ยนการชำระ กดหมายเลย '-1");
        }
      } else if (payment1 == '3') {
        print("ใช้เต่าบินเครดิต");
        money1 = int.parse(stdin.readLineSync()!);
        if (money1 >= sum_cart) {
          print("ชำระเงินสำเร็จ");
          break;
        } else {
          print(
              "จำนวนเงินของท่านไม่เพียงพอ กรุณาชำระเงินใหม่ (โดยเปลี่ยนการชำระ กดหมายเลย '-1");
        }
      } else if (payment1 == '4') {
        print("ใช้คูปอง/ดูคูปองสะสม");
        money1 = int.parse(stdin.readLineSync()!);

        if (money1 >= sum_cart) {
          print("ชำระเงินสำเร็จ");
          break;
        } else {
          print(
              "จำนวนเงินของท่านไม่เพียงพอ กรุณาชำระเงินใหม่ (โดยเปลี่ยนการชำระ กดหมายเลย '-1");
        }
      }
    }
    if (money1 != -1) break;
  }
}
